document.addEventListener("DOMContentLoaded", function () {

    const slider = document.querySelector('.slider');
    const slides = document.querySelectorAll('.slide');
    const prevButton = document.getElementById('prev');
    const nextButton = document.getElementById('next');
    let slideIndex = 0;

    function showSlide(index) {
        slides.forEach((slide, i) => {
            slide.style.transform = `translateX(${100 * (i - index)}%)`;
        });
    }

    prevButton.addEventListener('click', () => {
        slideIndex = Math.max(slideIndex - 1, 0);
        showSlide(slideIndex);
    });

    nextButton.addEventListener('click', () => {
        slideIndex = (slideIndex + 1) % slides.length;
        showSlide(slideIndex);
    });

    showSlide(slideIndex);

});