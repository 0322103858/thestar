document.addEventListener('DOMContentLoaded', function() {
    const form = document.getElementById('payment-form');
  
    form.addEventListener('submit', function(event) {
      event.preventDefault(); // Evitar el envío del formulario por defecto
  
      //  valores de los campos
      const cardNumber = document.getElementById('card-number').value;
      const cardHolder = document.getElementById('card-holder').value;
      const expirationDate = document.getElementById('expiration-date').value;
      const securityCode = document.getElementById('security-code').value;
  
      //  validación de los datos, como verificar el número de tarjeta,
   
      if (cardNumber.trim().length !== 19 || cardHolder.trim() === '' || expirationDate.trim().length !== 5 || securityCode.trim().length !== 4) {
        alert('Por favor, completa todos los campos correctamente.');
        return; // Detener el procesamiento si los campos no están completos o son inválidos
      }
  
    

      fetch('tu-servidor/procesar-pago', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cardNumber,
          cardHolder,
          expirationDate,
          securityCode,
        }),
      })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Error en la solicitud');
        }
      })
      .then(data => {
      
        console.log('Pago procesado:', data);
        alert('¡Pago procesado con éxito!');
      })
      .catch(error => {

        console.error('Error:', error);
        alert('Ha ocurrido un error al procesar el pago.');
      });
    });
  });
  
 