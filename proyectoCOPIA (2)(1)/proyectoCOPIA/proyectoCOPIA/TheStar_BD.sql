---THE STAR

create database thestar_bd

create table cuenta (
    username varchar (15) primary key,
    email varchar (50) unique,
    contraseña varchar (15) not null
)

create table cliente (
    numero int primary key,
    nombre varchar (30) not null,
    apellidoP varchar (15) not null,
    apellidoM varchar (15) not null,
    numTel varchar (15) unique,
    username varchar (15),
    foreign key (username) references cuenta (username)
)

create table pagos (
    numero int primary key,
    fecha date not null,
    montoTotal float not null,
    modopago varchar(5) not null
)

create table pedidos (
    codigo varchar (15) primary key,
    fechaInicio date not null,
    fechaCierre date not null,
    Estado varchar (30) not null,
    Municipio varchar (30) not null,
    cp varchar (5) not null,
    colonia varchar (30) not null,
    calle varchar (30) not null,
    cliente int,
    pagos int,
    foreign key (cliente) references cliente (numero),
    foreign key (pagos) references pagos (numero)
)

create table proveedores (
    numero int primary key,
    nombre varchar (30) not null,
    apellidoPaterno varchar (30) not null,
    apellidoMaterno varchar (30), 
    numTelefono varchar (15) unique,
    correo varchar (50) unique
)

create table categorias (
    codigo varchar (5) primary key,
    nombre varchar (30) not null
)

create table colecciones (
    codigo varchar (5) primary key,
    nombre varchar (30) not null,
    descripcion varchar (200) not null,
    categoria varchar (5),
    foreign key (categoria) references categorias (codigo)
)

create table productos (
    codigo varchar (5) primary key,
    nombre varchar (50) not null,
    descripcion varchar (250) not null,
    precio float not null,
    stock int (5) not null,
    colecciones varchar (5) not null,
    foreign key (colecciones) references colecciones (codigo)
)

create table proveedor_coleccion ( 
    productos varchar (5),
    proveedores int (5),
    primary key (productos,proveedores),
    foreign key (productos) references colecciones (codigo),
    foreign key (proveedores) references proveedores (numero)
)

create table pedidos_productos (
    pedidos varchar (15),
    productos varchar (5),
    primary key (pedidos, productos),
    foreign key (pedidos) references pedidos (codigo),
    foreign key (productos) references productos (codigo)
)

insert into cuenta values
('ErikaJuana12',	'erika.juana@gmail.com',	'Neli77'),
('LuciPretty',	'lucia.montserrat2@gmail.com',	'holis45'),
('AimaraFelix1',	'aimara.julissa67@gmail.com',	'Holimoli88'),
('HadeyHassel',	'hadey.hassel7@gmail.com',	'aiuda89'),
('JosRosas',	'joselyn.rosas77@gmail.com',	'Holamundo45'),
('KarelySanchezz',	'karla.karelyy@gmail.com',	'Marcos34'),
('HailyyB',	'haily.anaa12@gmail.com',	'cpaput12'),
('RositaSofi',	'rosa.sofia89@gmail.com',	'Mikupro111'),
('SteicyRodrzz',	'steicy.basilio11@gmail.com',	'Lupita21'),
('LupitaLuz',	'guadalupe.luz@gmail.com',	'Python1999')


insert into cliente values
('1',	'Erika Juana',	'Martinez',	'Rodriguez',	'664-234-6989',	'ErikaJuana12'),
('2',	'Lucia Montserrat',	'Duran',	'Salinas',	'664-892-2345',	'LuciPretty'),
('3',	'Aimara Julissa',	'Felix',	'Gastelo',	'663-092-2332',	'AimaraFelix1'),
('4',	'Hadey Hassel',	'Zamora',	'Sainz',	'663-122-4244',	'HadeyHassel'),
('5',	'Joselyn',	'Aispuro',	'Rosas',	'664-323-1213',	'JosRosas'),
('6',	'Karla Karely',	'Rodriguez',	'Sanchez',	'664-232-9373',	'KarelySanchezz'),
('7',	'Haily Ana',	'Blanco',	'Luna',	'664-765-2390',	'HailyyB'),
('8',	'Rosa Sofia',	'Roman',	'Herrera',	'664-898-4211',	'RositaSofi'),
('9',	'Steicy',	'Basilio',	'Rodriguez',	'664-100-9898',	'SteicyRodrzz'),
('10',	'Guadalupe Luz',	'Padilla',	'Renteria',	'663-367-9823',	'LupitaLuz')

insert into pagos values
(1, '2023-02-09', 350, 'TRSFC'),
(2, '2023-08-09', 500, 'TJCRD'),
(3, '2023-09-19', 400, 'TRSFC'),
(4, '2023-09-20', 450, 'TRSFC'),
(5, '2023-09-23', 650, 'TRSFC'),
(6, '2023-10-07', 700, 'TRSFC'),
(7, '2023-10-14', 500, 'TJDEB'),
(8, '2023-10-20', 350, 'TJCRD'),
(9, '2023-10-20', 300, 'TJCRD'),
(10, '2023-10-20', 350, 'TJCRD')

insert into pedidos values 
('987650',	'2023-02-09',	'2023-02-19',	'Sinaloa',	'Culiacan',	'34563',	'Centro',	'Calle 4ta',	'1',	'1'),
('987651',	'2023-08-09',	'2023-08-19',	'Sonora',	'Hermosillo',	'27456',	'Santa Fe',	'Oriente',	'2',	'2'),
('987652',	'2023-09-19',	'2023-09-29',	'Guerrero',	'Chilpancingo',	'36678',	'Industrial',	'Blvd. López Mateos',	'3',	'3'),
('987653',	'2023-09-20',	'2023-09-30',	'Nayarit',	'Tepic',	'11675',	'Parcelas',	'Calle 6ta',	'4',	'4'),
('987654',	'2023-09-23',   '2023-09-30',	'Baja California',	'Ensenada',	'11114',	'San Francisco',	'Blvd.Real de Baja California',	'5',	'5'),
('987655',	'2023-10-07',	'2023-10-17',	'Yucatan',	'Merida',	'66323',	'Reforma',	'Av. Reforma',	'6',	'6'),
('987656',	'2023-10-14',	'2023-10-24',	'Jalisco',	'Guadalajara',	'22213',	'Santa Lucía',	'Carr. Transpeninsular',	'7',	'7'),
('987657',	'2023-10-20',	'2023-10-30',	'Nuevo Leon',	'Monterrey',	'67888',	'Centro',	'Av. Ruíz',	'8',	'8'),
('987658',	'2023-10-20',	'2023-10-30',	'Michoacan',	'Morelia',	'42322',	'Zona Centro',	'Av. López Mateos',	'9',	'9'),
('987659',	'2023-10-20',	'2023-10-30',	'Veracruz',	'Xalapa',	'34455',	'Fracc. Villa  Residencial',	'Blvd. Anahuac',	'10',	'10')

insert into proveedores values 
('1',	'Leonor Rocio',	'Vargas',	'Casas',	'664-234-6990',	'leonor.rocio235@gmail.com'),
('2',	'Patricia Ann',	'Real',	'Ruiz',	'664-892-2346',	'patricia.annC@gmail.com'),
('3',	'Teresa Jessica',	'Silva',	'Reyes',	'663-092-2333',	'teresa.jessicaSR@gmail.com'),
('4',	'Maria Dulce',	'Garcia',	'Vargas',	'663-141-1017',	'maria.dulceMD8@gmail.com'),
('5',	'Edith Hania',	'Perez',	'Cuevas',	'664-377-1214',	'edith.hania765@gmail.com'),
('6',	'Alex Juan',	'Morales',	'Lopez',	'664-232-9374',	'alex.juan14@gmail.com'),
('7',	'Eduardo Luis',	'Cuevas',	'Hipolito',	'664-765-2391',	'eduardo.luis34@gmail.com'),
('8',	'Ivan Kevin',	'Baltazar',	'Soriano',	'664-898-4212',	'ivan.kevinn9@gmail.com'),
('9',	'Elizabeth Kenia',	'Cruz',	'Padua',	'664-148-6674',	'elizabeth.keniaa@gmail.com'),
('10',	'Vanesa Luisa',	'Manriquez',	'Fernandez',	'663-205-9844',	'vanesa.luisaa@gmail.com')


insert into categorias values  
('LOCI',	'Lociones'),
('ACCE',	'Accesorios'),
('MAQUI',	'Maquillaje')

insert into colecciones values  
('CARD',	'House of Card',	'Colección de maquillaje y lociones inspirada en la baraja inglesa.',	'LOCI'),
('ETER',	'Ethereal',	'Colección de accesorios de plata 9.25',	'ACCE'),
('MAGL',	'Magic Glam',	'Colección de maquillaje y lociones inspirada en la magia del glamour.',	'MAQUI')

insert into productos values 
('LRCZ',	'Locion reina de corazones',	'Fragancia de vainilla dulce, ciruela y coco con lirios, jazmín, nardos y rosas.',	'350',	'50',	'CARD'),
('LRPS',	'Locion rey de picas',	'Fragancia de Resinas, inciensos, lavanda, pachulI, vetiver, bergamota, geranio, bayas rosas y pimienta de Sichuan',	'500',	'50',	'CARD'),
('DILN',	'Diamond locion',	'Fragancia de  limón italiano y mandarina con hojas verdes machacadas, evocando la frescura de la savia recién brotada, y un desafiante golpe de pimienta rosa.',	'400',	'50',	'CARD'),
('SREI',	'Set reinas',	'Fragancia de café, vainilla y pera. Incluye muestras de otras fragancias de la colección.',	'450',	'50',	'CARD'),
('SREY',	'Set reyes',	'Fragancia de cedro,  jengibre,  violeta,  vetiver,  cedro y sándalo. Incluye loción rey de treboles y locion rey de corazones',	'650',	'50',	'CARD'),
('ROYA',	'Royalty',	'Fragancia de rosas blancas, flor de azahar, almizcle y madera de sándalo.'	,'700',	'50',	'CARD'),
('POKE',	'Poker set',	'Fragancia de ámbar vainilla, con notas de café, haba tonka, vainilla y algunas flores orientales. Incluye 2 pares de pupilentes, 7 sombras en colores metálicos e iluminadores',	'500',	'50',	'CARD'),
('SHAS',	'Shadow As',	'Paleta de sombras de colores metálicos.',	'350',	'50',	'CARD'),
('REPI',	'Reina de picas',	'Juego de sombras de colores metálicos (negro, plateado y azul) junto con un iluminador plateado.',	'300',	'50',	'CARD'),
('SHST',	'Shiny star',	'Aretes de plata 9.25 con zirconia.',	'350',	'50',	'ETER'),
('SIMO',	'Sirius Moon',	'Aretes de plata 9.25 con zirconia.',	'500',	'50',	'ETER'),
('STMO',	'Star y moon',	'Aretes de plata 9.25 con zirconia.',	'480',	'50',	'ETER'),
('STNI',	'Starry night',	'Aretes de plata 9.25 con zirconia.',	'250',	'50',	'ETER'),
('CAPE',	'Capella',	'Aretes de plata 9.25 con zirconia.',	'200',	'50',	'ETER'),
('ZAFI',	'Zafiro',	'Set de aretes de plata 9.25 con zirconia y zafiro.',	'400',	'50',	'ETER'),
('URAN',	'Uranus',	'Aretes de plata 9.25 con perlas.',	'200',	'50',	'ETER'),
('BUTF',	'Butterfly',	'Aretes de plata 9.25 con zirconia y zafiros.',	'250',	'50',	'ETER'),
('DIAM',	'Diamond waterfall',	'Aretes de plata 9.25 con zirconia y cuarzo.',	'200',	'50',	'ETER'),
('CONS',	'Constelacion',	'Aretes de plata 9.25 con zirconia.',	'300',	'50',	'ETER'),
('SETZ',	'Set Zafiro',	'Set de 6 anillos  de plata 9.25 de diferentes diseños y piedras de zafiro.',	'960',	'50',	'ETER'),
('BRIG',	'Bright',	'Set de collares luna y estrella, dos piezas de plata 9.25 con piedras incrustadas de circonita.', 	'350',	'50',	'ETER'),
('SAZZ',	'Set azul zafiro',	'set de anillos de plata 9.25 con piedras de zafiro incrustadas, contiene 7 piezas.' ,	'1000'	,'50',	'ETER'),
('GOLM',	'Golden mirror',	'Set de anillos de 3 piezas, hechas de plata 9.25 vermeil con obsidianas. ',	'600',	'50',	'ETER'),
('BLIN',	'Bling',	'Set de 6 piezas, incluye 2 brazaletes, 3 anillos y un collar todos hechos de plata 9.25' ,	'700',	'50',	'ETER'),
('DGOL',	'10dGold',	'Anillo de plata vermeil 9. 25 con piedra de obsidiana','350','50','ETER'),
('CHAI',	'Silver chain',	'cadena barbada de plata 9. 25','250', '50','ETER'),
('CDPL',	'Cadena platino',	'cadena Adige de plata 9. 25 con cuarzo',	'350',	'50',	'ETER'),
('OBRI',	'Osidian ring',	'Anillo tipo espejo de obsidiana',	'400',	'50',	'ETER'),
('POSE',	'Praesidium',	'Fragancia de flor de naranja, mandarina, vetiver,  notas frescas de comino, te negro, pachulí, ámbar y ládano.',	'700',	'50',	'MAGL'),
('SLAB',	'Set labil',	'Set de labiales incluye 3 tonos diferentes.',	'650',	'50',	'MAGL'),
('EPIP',	'Epiphany',	'Fragancia de bergamota italiana, jazmín, vainilla y una nota de maderas frescas con toques cálidos.',	'850',	'50',	'MAGL'),
('ZODI',	'Zodiacal',	'set de 12 sombras'	,'550',	'50',	'MAGL'),
('MOON',	'Moonlight star',	'Set de delineadores con glitter (plata, negro, dorado)',	'300',	'50',	'MAGL'),
('CRYST',	'Crystal night',	'Set de dos brochas.',	'250',	'50',	'MAGL'),
('LUAR',	'Luar locion',	'Fragancia de almizcles amaderados,  pachuli,  sándalo y  vainilla.',	'650',	'50',	'MAGL'),
('AMOL',	'Amore locion',	'Fragancia de polvos y almizcle.',	'800',	'50',	'MAGL'),
('STLO',	'Star locion',	'Fragancia de cítricos, flores y olivo,  ámbar, benjuí, almizcle y madera de gaiac.',	'700',	'50',	'MAGL'),
('GOLD',	'Golden moon',	'set, incluye un polvo translucido, un polvo de color, iluminadores y dos labiales.',	'650',	'50',	'MAGL'),
('MAGI',	'Night magic',	'Incluye productos de la coleccion magic Glam: Iluminadores, brochas, paletas de sombras, rubores, delineadores,  lociones Luar y labiales.',	'2500',	'50',	'MAGL'),
('YOUR',	'Set yourself',	'set de 7 piezas, incluye vela, iluminador liquido, base, polvo translucido,  2 lociones, set de labiales y sombras.',	'1500',	'50',	'MAGL'),
('POLS',	'Polvo sirena',	'Polvo compacto  con un olor dulce y fresco' ,	'200',	'50',	'MAGL'),
('MOME',	'Moonlight Mermaid',	'Paleta de sombras de tonos brillantes y metalicos.',	'300',	'50',	'MAGL'),
('STRM',	'Starry Moonlight',	'Iluminador con polvo de estrella y olor dulce.',	'250',	'50',	'MAGL')


insert into proveedor_coleccion values 
('CARD',	'1'),
('ETER',	'2'),
('MAGL',	'3')

insert into pedidos_productos values 
('987650',	'LRCZ'),
('987651',	'LRPS'),
('987652',	'DILN'),
('987653',	'SREI'),
('987654',	'SREY'),
('987655',	'ROYA'),
('987656',	'POSE'),
('987657',	'SHAS'),
('987658',	'REPI'),
('987659',	'SHST')